package hu.dpc.edu.books.entity;

import java.util.function.Function;
import java.util.function.Predicate;

public class Predicates {

    public final static Predicate ALWAYS_MATCHES = object -> true;

    public static <T> Predicate<T> contains(Function<T, String> stringPropertyExtractor, String substring) {
        if (substring == null) {
            return ALWAYS_MATCHES;
        }
        return (T object) -> stringPropertyExtractor.apply(object)
                .contains(substring);
    }

    public static <T> Predicate<T> equalTo(Function<T, Object> propertyExtractor, Object value) {
        if (value == null) {
            return ALWAYS_MATCHES;
        }
        return (T object) -> propertyExtractor.apply(object)
                .equals(value);
    }
}
