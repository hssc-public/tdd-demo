package hu.dpc.edu.books.entity;

import java.util.function.Function;
import java.util.stream.Stream;

public class FilteredStreamBuilder<T> {
    private Stream<T> stream;

    public FilteredStreamBuilder(Stream<T> stream) {
        this.stream = stream;
    }

    public FilteredStreamBuilder<T>propertyContains(Function<T, String> stringPropertyExtractor, String substring) {
        if (substring != null) {
            this.stream = this.stream.filter(Predicates.contains(stringPropertyExtractor, substring));
        }
        return this;
    }

    public FilteredStreamBuilder<T>propertyEqualsTo(Function<T, Object> propertyExtractor, Object value) {
        if (value != null) {
            this.stream = this.stream.filter(Predicates.equalTo(propertyExtractor, value));
        }
        return this;
    }

    public Stream<T> build() {
        return stream;
    }
}
