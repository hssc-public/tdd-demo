package hu.dpc.edu.books.entity;

@FunctionalInterface
public interface IdGenerator<ID> {
    ID generateId();
}
