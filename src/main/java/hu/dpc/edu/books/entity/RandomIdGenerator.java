package hu.dpc.edu.books.entity;

import java.util.Random;

public class RandomIdGenerator implements LongIdGenerator {

    private Random random = new Random();

    @Override
    public long generateId() {
        return random.nextLong();
    }
}
