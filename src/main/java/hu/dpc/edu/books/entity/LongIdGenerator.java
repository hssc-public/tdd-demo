package hu.dpc.edu.books.entity;

@FunctionalInterface
public interface LongIdGenerator {
    long generateId();
}
