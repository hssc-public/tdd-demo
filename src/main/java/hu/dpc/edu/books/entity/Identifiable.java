package hu.dpc.edu.books.entity;

public interface Identifiable<ID> {
    ID getId();
}
