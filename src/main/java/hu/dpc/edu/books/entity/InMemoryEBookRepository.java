package hu.dpc.edu.books.entity;

import java.util.Map;

public class InMemoryEBookRepository extends InMemoryBookRepository
{
    public InMemoryEBookRepository(Map<Long, Book> bookById, LongIdGenerator idGenerator) {
        super(bookById, idGenerator);
    }

    @Override
    public ElectronicBook findById(long id) {
        return new ElectronicBook();
    }

    @Override
    public void update(Book book) {
        super.update(book);
    }

    @Override
    public void doIt() {

    }
}
