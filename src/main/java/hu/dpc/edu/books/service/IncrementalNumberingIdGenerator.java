package hu.dpc.edu.books.service;

import hu.dpc.edu.books.entity.LongIdGenerator;
import hu.dpc.edu.books.spring.MyComponent;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.concurrent.atomic.AtomicLong;

@MyComponent
public class IncrementalNumberingIdGenerator implements LongIdGenerator, InitializingBean, DisposableBean {

    private AtomicLong counter = new AtomicLong();

    @Override
    public long generateId() {
        return counter.incrementAndGet();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("after properties set...");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroying IncrementalNumberingIdGenerator");
    }
}
