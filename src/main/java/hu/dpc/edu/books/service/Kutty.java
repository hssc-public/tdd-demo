package hu.dpc.edu.books.service;

import hu.dpc.edu.books.spring.MyDateFomats;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
public class Kutty {

  private ThreadLocal<DateFormat>dateFormat = new ThreadLocal<>() {
    @Override
    protected DateFormat initialValue() {
      return new SimpleDateFormat("YYYY-MM-dd");
    }
  };
  private ThreadLocal<DateFormat>timeFormat = new ThreadLocal<>() {
    @Override
    protected DateFormat initialValue() {
      return new SimpleDateFormat("HH:mm:ss");
    }
  };

  @Bean
  @Scope("prototype")
  @MyDateFomats(MyDateFomats.FORMAT.DATE)
  public DateFormat dateFormat() {
    return dateFormat.get();
  }

  @Bean
  @Scope("prototype")
  @MyDateFomats(MyDateFomats.FORMAT.TIME)
  public DateFormat timeFormat() {
    return timeFormat.get();
  }
}
