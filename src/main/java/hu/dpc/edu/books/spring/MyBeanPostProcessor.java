package hu.dpc.edu.books.spring;

import hu.dpc.edu.books.entity.RandomIdGenerator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {
  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    System.out.println("postProcessAfterInitialization, beanName = " + beanName);
    if ("incrementalNumberingIdGenerator".equals(beanName)) {
      return new RandomIdGenerator();
    }
    return bean;
  }

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
    System.out.println("postProcessBeforeInitialization, beanName = " + beanName);
    return bean;
  }
}
