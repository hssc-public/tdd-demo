package hu.dpc.edu.books.spring;

import hu.dpc.edu.books.entity.*;
import hu.dpc.edu.books.service.Kutty;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

import java.util.HashMap;
import java.util.Random;

@Configuration
@ComponentScan(basePackageClasses = Kutty.class)
public class AppConfig {

  private Random random = new Random();

  @Bean
  @Scope("prototype")
  Long randomNumber() {
    return (long)(random.nextInt(1000));
  }

  @Bean
  @BookRepositoryCapacity
  Long capacity() {
    return 100L;
  }

//  @Bean
//  MyBeanPostProcessor myBeanPostProcessor() {
//    return new MyBeanPostProcessor();
//  }
//
//  @Bean
//  MyBeanFactoryPostProcessor myBeanFactoryPostProcessor() {
//    return new MyBeanFactoryPostProcessor();
//  }

  @Bean
  RandomGenerator randomGenerator() {
    return new RandomGenerator();
  }

  @Bean()
  @Lazy
  public InMemoryBookRepository inMemoryBookRepository(LongIdGenerator idGenerator) {
    return new InMemoryBookRepository(new HashMap<>(), idGenerator);
  }

}
