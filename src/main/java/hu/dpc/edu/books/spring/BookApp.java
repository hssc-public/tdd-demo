package hu.dpc.edu.books.spring;

import hu.dpc.edu.books.entity.Book;
import hu.dpc.edu.books.entity.InMemoryBookRepository;
import hu.dpc.edu.books.entity.LongIdGenerator;
import hu.dpc.edu.books.spring.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class BookApp {
  public static void main(String[] args) {
    try (final AnnotationConfigApplicationContext context
                 = new AnnotationConfigApplicationContext(AppConfig.class)) {

      final LongIdGenerator idGenerator = context.getBean(LongIdGenerator.class);
      System.out.println(idGenerator.getClass());
      System.out.println(idGenerator.generateId());
      System.out.println(idGenerator.generateId());

      final InMemoryBookRepository bookRepository = context.getBean(InMemoryBookRepository.class);
      final Book book1 = new Book("Title", "Author");
      final long book1Id = bookRepository.add(book1);

      final List<Book> all = bookRepository.findAll();
      System.out.println(all);

      System.out.println(context.getBean("randomNumber"));
      System.out.println(context.getBean("randomNumber"));
      System.out.println(context.getBean("randomNumber"));
      System.out.println(context.getBean("randomNumber"));

      final RandomGenerator randomGenerator = context.getBean(RandomGenerator.class);
      System.out.println("randomGenerator:");
      System.out.println(randomGenerator.generate());
      System.out.println(randomGenerator.generate());
      System.out.println(randomGenerator.generate());
    }
  }
}
