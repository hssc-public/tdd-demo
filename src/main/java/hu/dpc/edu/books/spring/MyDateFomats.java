package hu.dpc.edu.books.spring;

import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Qualifier
public @interface MyDateFomats {

  FORMAT value();

  public enum FORMAT {
    FULL,
    DATE,
    TIME
  }
}
