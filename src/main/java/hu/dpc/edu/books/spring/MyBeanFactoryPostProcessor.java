package hu.dpc.edu.books.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.util.Iterator;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor{
  @Override
  public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
    System.out.println("postProcessBeanFactory");

    for(final Iterator<String> it = configurableListableBeanFactory.getBeanNamesIterator();
        it.hasNext();) {
      final String beanName = it.next();
      System.out.println("Bean found: " + beanName);
    }
  }
}
