package hu.dpc.edu.books.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

public class RandomGenerator implements BeanFactoryAware{

  private BeanFactory beanFactory;
  @Override
  public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    this.beanFactory = beanFactory;
  }

  public long generate() {
    return beanFactory.getBean("randomNumber",Long.class);
  }
}
