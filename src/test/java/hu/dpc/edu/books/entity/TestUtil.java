package hu.dpc.edu.books.entity;

import org.junit.jupiter.api.Assertions;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;

import java.util.Objects;

public class TestUtil {

    public static void assertBookEquals(Book expected, Book actual) {
        Assertions.assertEquals(expected.getId(), actual.getId());
        Assertions.assertEquals(expected.getTitle(), actual.getTitle());
        Assertions.assertEquals(expected.getAuthor(), actual.getAuthor());
    }

    public static ArgumentMatcher<Book> bookEqualsTo(Book expected) {
        return (book) -> Objects.equals(expected.getId(), book.getId())
                && Objects.equals(expected.getTitle(), book.getTitle())
                && Objects.equals(expected.getAuthor(), book.getAuthor());
    }

    public static <T> ArgumentMatcher<T> notSame(T notExpectedReference) {
        return (T value) -> notExpectedReference != value;
    }
}
