package hu.dpc.edu.books.entity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;

@Configuration
public class TestConfig {

  @Bean LongIdGenerator idGenerator() {
    return mock(LongIdGenerator.class);
  }

  @Bean
  public Map<Long,Book> bookRepositoryMap() {
      return new HashMap<>();
  }

  @Bean
  public InMemoryBookRepository inMemoryBookRepository(Map<Long,Book> bookRepositoryMap, LongIdGenerator idGenerator) {
    return new InMemoryBookRepository(bookRepositoryMap, idGenerator);
  }

}
