package hu.dpc.edu.books.entity;

import hu.dpc.edu.books.spring.AppConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class InMemoryBookRepositoryIntegrationTest {

  private Book book;
  private Book expectedBookToReturn;

  @Autowired
  private LongIdGenerator idGenerator;
  @Autowired
  private InMemoryBookRepository bookRepository;

  @Autowired
  private Map<Long,Book> bookRepositoryMap;


  @BeforeEach
  void beforeEach() {
    bookRepositoryMap.clear();
    given(idGenerator.generateId())
            .willReturn(35L);

    book  = new Book("valami", "Szerzo");

    expectedBookToReturn = new Book(35L,"valami", "Szerzo");
  }

  @Test
  void testAdd() {
    final long returnedId = bookRepository.add(book);
    final Book returnedBook = bookRepository.findById(returnedId);

    assertEquals(expectedBookToReturn, returnedBook);
  }

  @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
  @Test
  void shouldBeEmpty() {
    final List<Book> all = bookRepository.findAll();

    assertEquals(0, all.size());
  }
}