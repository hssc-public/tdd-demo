package hu.dpc.edu.books.entity;

import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ComparatorTest {

    @Test
    public void testComparing() {
        Comparator<Book> byAuthor = Comparator.comparing(book -> book.getAuthor());

        final Book book1 = new Book("Cim1", "Szerzo2");
        final Book book2 = new Book("Cim2", "Szerzo1");

        final int result = byAuthor.compare(book1, book2);

        assertTrue(result > 0);

        final int result2 = byAuthor.reversed().compare(book1, book2);

        assertTrue(result2 < 0);

    }
}
