package hu.dpc.edu.books.entity;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ImmutableListTest {

    @Test
    public void testImmutable() {
        final List<String> list = new ArrayList<>();
        list.add("ABC");
        list.add("DEF");
        final List<String> unmodifiableList = Collections.unmodifiableList(list);

        assertIterableEquals(List.of("ABC","DEF"), unmodifiableList);

        assertThrows(UnsupportedOperationException.class, () -> {
            unmodifiableList.add("EEE");
        });

        list.add("ALMA");

        assertIterableEquals(List.of("ABC","DEF", "ALMA"), unmodifiableList);

    }
}
