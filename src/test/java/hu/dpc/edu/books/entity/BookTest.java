package hu.dpc.edu.books.entity;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

class BookTest {

    private Book book = new Book("Cim", "Szerzo");

    private Book spy = Mockito.spy(book);


    @Test
    public void test() {

        String title = spy.getTitle();
        assertEquals("Cim", title);
    }

    @Test
    public void test2() {

        given(spy.getTitle())
                .willReturn("Cim2");

        String title = spy.getTitle();
        assertEquals("Cim2", title);
    }
}